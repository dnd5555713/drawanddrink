module.exports = {
  purge: ['./src/App.vue', './public/index.html'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {},
  },
  variants: {
    extend: {},
  },
  plugins: [],
  options: {
    safelist: ['bg-darkest'],
  },
};
